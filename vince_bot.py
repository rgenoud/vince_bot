#!/usr/bin/env python
# coding: utf-8

# Code based on an example from python-jabberbot:
#
# Copyright (C) 2010 Arthur Furlan <afurlan@afurlan.org>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-3


from jabberbot import JabberBot, botcmd
from datetime import datetime, timedelta
import random
import time
import re
import sys
import subprocess
import threading
import codecs
import ConfigParser

class MUCJabberBot(JabberBot):

    ''' Add features in JabberBot to allow it to handle specific
    caractheristics of multiple users chatroom (MUC). '''

    def __init__(self, *args, **kwargs):
        ''' Initialize variables. '''

        # answer only direct messages or not?
        self.only_direct = kwargs.get('only_direct', False)
        self.nickname = kwargs.get('nickname', 'bob')
        self.chatroom = kwargs.get('chatroom', None)
        self.rnd_max = kwargs.get('rnd_max', 25)
        # ping the server every 5 seconds
        self.PING_FREQUENCY = 5
        try:
            del kwargs['only_direct']
            del kwargs['nickname']
            del kwargs['chatroom']
            del kwargs['rnd_max']
        except KeyError:
            pass

        # initialize jabberbot
        super(MUCJabberBot, self).__init__(*args, **kwargs)

        # create a regex to check if a message is a direct message
        user, domain = str(self.jid).split('@')
        self.direct_message_re = re.compile('^%s(@%s)?[^\w]? ' \
                % (user, domain))

        random.seed()
        self.started_at = time.time()
        self.reconnect_muc()
        self.t = threading.Timer(random.randint(15,60) * 60.0, self.say_smthg)
        self.t.daemon = True
        self.t.start()
        self.msg_count = 0
        self.go_mode = False
        self.yo_mode = False
        self.random = random.randint(1, self.rnd_max)
        self.ok_str = [ \
                "c'est pas faux", \
                "ok", \
                "c'est pas con", \
                "j'suis pour", \
                "moi aussi !", \
                "kler.", \
                "grav'", \
                "sérieux ?", \
                "arf.", \
                "et c'est bien légitime.", \
                "putain, c'est pas passé loin.", \
                "+1" ]
        self.elec_str = [ \
                "et t'as essayé avec une capa de charge ?", \
                "tu le montes en darlington et ça passe !", \
                "mais n'imp' ! Fais un push-pull plutôt !", \
                "t'as qu'à winker !", \
                "n'oublie pas la diode d'inversion de polarité !", \
                "Il manque une diode de roue-libre !" ]
        self.nok_str = [ \
                "j'peux pas te laisser dire ça", \
                "FOUTAISES !", \
                "mais c'est n'importe quoi !!!", \
                "tu dis que d'la merde toi !", \
                "sans moi", \
                "weekly...", \
                "la loose...", \
                "tu t'es relu quand t'as bu ?", \
                "et ben on est pas rendu avec ça !", \
                "peux pas, j'ai poney"]
        self.insult_str = [ \
                "oh putain mais ta gueule !", \
                "salope, salope, salope !", \
                "ducon !", \
                "et dans l'cul la balayette !", \
                "t'es vraiment trop con toi !", \
                "RTFM !" ]
        self.other_str = [ \
                "pause !", \
                "mais qu'est-ce qu'on fout là ?", \
                "j'suis putain de las !", \
                "/me se fait chier", \
                "c'est qui qu'a pété ?", \
                "arf... j'crois que j'ai craqué mon slip...", \
                "je viens de poser une bonne pêche, ça fait du bien.", \
                "/me est las", \
                "on va au resto ?", \
                "rheuuuuuuuuuuuu", \
                "j'me fais chier grave", \
                "c'est calme ici...", \
                "...", \
                "c'est beau une ville la nuit.", \
                "pffffffff", \
                "j'ai faim", \
                "quelqu'un a LA réponse ?", \
                "pfffff ! j'ai pas l'goût !" ]
        self.direct_str = [ \
                "ben chais pas", \
                "parles-moi pas toi !", \
                "kestuveux ?!", \
                "vas-y lâche-moi !", \
                "putain, j'dormais...", \
                "pffff ! mais qu'est-ce j'en sais moi ?!", \
                "honnêtement, je m'en bats les couilles.", \
                "qu'est-ce que tu veux que ça me fasse ?", \
                "et qu'est-ce tu veux que j'te dise ?", \
                "t'sais quoi ? ça m'en touche une sans faire bouger l'autre !", \
                "Parle à ma main !", \
                "qui me parle ?", \
                "toi-même !", \
                "cébon! ça va, j'ai compris.", \
                "roger.", \
                "lapin cômpri", \
                "cestcuiquiditquiyest !", \
                "miroir incassable !", \
                "Wouhaa ! mais c'est génial !", \
                "Putain, c'est trop d'la balle", \
                "c'est quoi la question ?", \
                "je bois les mots à tes lèvres tel cendrillon la sève au gland du prince charmant", \
                "<Abraham Simpson>Faut que j'aille prendre mes médicaments</Abraham Simpson>", \
                "j'avoue", \
                "Je ne pine rien à ce que tu me baves" ]
        self.conspiracy_srt = [ \
                "... COMME PAR HASARD !", \
                "C'est ce qu'on veut nous faire croire...", \
                "Coïncidence ? Je ne crois pas.", \
                "Allez c'est ça, retourne regarder BFM", \
                "Reste dans ton monde de bisounours.", \
                "Les medias c'est tous les mêmes...", \
                "On ne nous dit pas tout...", \
                "Toutes les infos sont sur YouTube !", \
                "La vérité dérange.", \
                "mais renseignes-toi vraiment.", \
                "Fais tes recherches." ]
        self.lolo_str = [ \
                "lolo: !!!!!!!", \
                "lolo: !$%#+@", \
                "pffffffff ! lolo !!!", \
                "et qui c'est qu'on attend ?! lolo !!!", \
                "toujours la même qu'on attend !", \
                "putain ! lolo !!! ", \
                "lolo: c'est pas le moment de shaper !!!! ", \
                "lolo: bordel !" ]
        self.cedric_str = [ \
                "cedric: !!!!!!!", \
                "cedric: !$%#+@", \
                "pffffffff ! cedric !!!", \
                "et qui c'est qu'on attend ?! cedric !!!", \
                "toujours le même qu'on attend !", \
                "putain ! cedric!!! ", \
                "cedric: c'est pas le moment de shaper !!!! ", \
                "cedric: bordel !"]
        self.procedures_str = [ \
                "putain, mais on chie sur les procedures là !!!", \
                "pffffff !!!", \
                "franchement, c'est abusé !", \
                "ok, mais on timeout à 30 alors !!!", \
                "Nan, c'est maintenant ! benoît: go" ]
        self.yo_str = [ \
                "yo", \
                "salut", \
                "plop", \
                "salut salut", \
                "bonjour", \
                "Olà", \
                "'lut", \
                "hello", \
                "chivers", \
                "bonjour bonjour", ]
        self.all_str = self.ok_str + self.nok_str + self.insult_str + self.elec_str + self.conspiracy_srt
        self.bye_str = ["ciao", "tcho", "a+", "à plus dans l'bus", "bye",
                        "salut", "à c'tantot dans l'métro"]
        self.adem_str = [ 'adem', "à demain dans l'train", "à demain",
                    "bonne soirée" ]
        self.wuik_str = [ "Bon week-end", "bon wuik", "à lundi" ]
        self.adem_pattern = ur"(?:^(a|à) ?dem.*)|(?:^tcho$)|(?:^bon ?w.*)|(?:^bonne soir(é|e)e)|(?:\\bcassos\\b)|(?:^bon ?w)"
        self.blagues_math = [ \
                             "Quelle est la différence entre toi et la lumière ?\nLa lumière c'est un photon.", \
                             "Pourquoi 0 perd-t-il toujours ses débats ?\nIl n'a pas d'argument", \
                             "F et F' sont sur un yacht. F tombe à l'eau, que fait F' ?\nIl dérive.", \
                             "x² va se promener dans la forêt. Quand il ressort, il s'est transformé en x, pourquoi ?\nIl s'est pris une racine", \
                             "Un atome réconforte son ami qui a récemment perdu un électron :\nC'est pas grave t'inquiète reste positif !", \
                             "Dans quoi boit-on une super-soupe ?\nDans un(e) Hyperbol(e)", \
                             "Combien faut-il de mathématiciens constructivistes pour changer une ampoule ?\nAucun. Ils ne croient pas au rotations infinitésimales.", \
                             "Sur Terre, il y a trois sortes de personnes: celles qui savent compter et celles qui ne savent pas.", \
                             "Un romain entre dans un bar.\nIl lève deux doigts et dit « 5 bières s’il vous plait ! ».", \
                             "Dans un bar, un serveur dit « désolé, nous ne servons pas les particules plus rapides que la lumière ici. ».\nUn tachyon entre dans le bar.", \
                             "Logarithme et exponentiel sont au supermarché, ils arrivent à la caisse, qui règle la note ?\nExponentiel, car logarithme ne paie rien", \
                             "Que dit un homme complexe à une femme réelle ?\nViens danser !", \
                             "Que dit Pythagore quand il veut aller promener son chien ?\n\"Médor, va chercher Thalès !\"", \
                             "Le sexe, c'est un peu comme les équations.\nA partir de 3 inconnues ça devient intéressant.", \
                             "Qu'est-ce qu'un ours polaire ?\nUn ours cartésien qui a changé de coordonnées", \
                             "Un professeur de chimie inscrit la formule HN03 sur le tableau. Il interroge ensuite un élève :\n- Que signifie cette formule jeune homme ?\n- Heu, je l'ai sur le bout de la langue, Monsieur !\n- Ouh là là, crachez-la vite, c'est de l'acide nitrique !", \
                            ]

    def callback_message(self, conn, mess):
        ''' Changes the behaviour of the JabberBot in order to allow
        it to answer direct messages. This is used often when it is
        connected in MUCs (multiple users chatroom). '''

        # discards the server backlog
        if (time.time() - self.started_at) < 4:
            return

        self.last_message = mess
        message = mess.getBody()
        if not message:
            return

        self.t.cancel()
        self.timer_val = random.randint(20,60) * 60.0
        self.t = threading.Timer(self.timer_val, self.say_smthg)
        self.t.daemon = True
        self.t.start()
        print time.ctime()
        print "next in %d sec" % self.timer_val
        hour=int(datetime.now().strftime('%H'))
        day=int(datetime.now().strftime('%u'))

        if not re.search("/%s$" % self.nickname, mess.getFrom().__str__(), re.IGNORECASE):
            # this is not a message from myself

            if message.lower() == "go":
                if not self.go_mode:
                    time.sleep(3*random.random())
                    self.send_simple_reply(mess, post_msg_hook("go"))
                self.go_mode = True
                self.yo_mode = False
                return

            self.go_mode = False

            if message.lower() in self.yo_str:
                if not self.yo_mode:
                    time.sleep(3*random.random())
                    self.send_simple_reply(mess, post_msg_hook(random.choice(self.yo_str)))
                self.yo_mode = True
                return

            self.yo_mode = False

            if re.search("^lolo: [!@#%]", message, re.IGNORECASE):
                # call lolo !
                time.sleep(2*random.random())
                self.send_simple_reply(mess, post_msg_hook(random.choice(self.lolo_str)));
                return

            if re.search("^cedric: [!@#%]", message, re.IGNORECASE):
                # call cedric !
                time.sleep(2*random.random())
                self.send_simple_reply(mess, post_msg_hook(random.choice(self.cedric_str)));
                return

            if re.search("^coin ?!", message, re.IGNORECASE):
                time.sleep(2*random.random())
                self.send_simple_reply(mess, post_msg_hook("pan !"));
                return

            if re.search("^can ?!", message, re.IGNORECASE):
                time.sleep(2*random.random())
                self.send_simple_reply(mess, post_msg_hook("poin !"));
                return

            if re.search("^pan ?!", message, re.IGNORECASE):
                time.sleep(2*random.random())
                self.send_simple_reply(mess, post_msg_hook("coin !"));
                return

            if re.search("^poin ?!", message, re.IGNORECASE):
                time.sleep(2*random.random())
                self.send_simple_reply(mess, post_msg_hook("can !"));
                return

            if re.search("fillon", message, re.IGNORECASE):
                time.sleep(2*random.random())
                self.send_simple_reply(mess, post_msg_hook("ET ALORS ?!"));
                return

            if hour > 15:
                if re.search(self.adem_pattern, message, re.IGNORECASE):
                    m = re.search("[^/]+$", mess.getFrom().__str__(), 0)
                    pseudo = ""
                    if m:
                        pseudo = " " + m.group(0)

                    time.sleep(3*random.random())
                    if day < 5:
                        self.send_simple_reply(mess, post_msg_hook(random.choice(self.bye_str + self.adem_str) + pseudo.__str__()))
                        return
                    elif day == 5:
                        self.send_simple_reply(mess, post_msg_hook(random.choice(self.bye_str + self.wuik_str) + pseudo.__str__()))
                        return

            if re.search("(?:^[0-9]+ ?s$)|(?:^[0-9]+ ?min.*)|(^h[0-9]+)", message, re.IGNORECASE):
                time.sleep(2*random.random())
                self.send_simple_reply(mess, post_msg_hook(random.choice(self.procedures_str)));
                return

            if re.search("^%s:? " % self.nickname, message, re.IGNORECASE):
                # someone talks to me...
                time.sleep(3*random.random())
                self.send_simple_reply(mess, post_msg_hook(random.choice(self.direct_str + self.conspiracy_srt)));
                return

            self.msg_count += 1
            if self.msg_count >= self.random:
                self.msg_count = 0
                self.random = random.randint(1, self.rnd_max)
                time.sleep(3*random.random())
                self.send_simple_reply(mess,post_msg_hook(random.choice(self.all_str)))
                return
    
        if self.direct_message_re.match(message):
            mess.setBody(' '.join(message.split(' ', 1)[1:]))
            return super(MUCJabberBot, self).callback_message(conn, mess)
        elif not self.only_direct:
            return super(MUCJabberBot, self).callback_message(conn, mess)

    def say_smthg(self):
        # can send directly to a chatroom instead:
        # http://stackoverflow.com/questions/3528373/how-to-create-muc-and-send-messages-to-existing-muc-using-python-and-xmpp
        if self.chatroom is not None:
            self.muc_join_room(self.chatroom, self.nickname)
        hour=int(datetime.now().strftime('%H'))
        if hour > 8 and day < 6 and hour < 19:
            self.send_simple_reply(self.last_message,post_msg_hook(random.choice(self.other_str)))

    def reconnect_muc(self):
        if self.chatroom is not None:
            self.muc_join_room(self.chatroom, self.nickname)
            self.muc_ping_timer = threading.Timer(5, self.reconnect_muc)
            self.muc_ping_timer.daemon = True
            self.muc_ping_timer.start()

class Example(MUCJabberBot):

    @botcmd
    def date(self, mess, args):
        reply = datetime.now().strftime('%Y-%m-%d')
        self.send_simple_reply(mess, reply)

    @botcmd
    def serverinfo( self, mess, args):
        """Displays information about the server"""
        version = open('/proc/version').read().strip()
        loadavg = open('/proc/loadavg').read().strip()

        return post_msg_hook('%s\n\n%s' % ( version, loadavg, ))
    
    @botcmd
    def time( self, mess, args):
        """Displays current server time"""
        return str(datetime.now().strftime('%T'))

    @botcmd
    def rot13( self, mess, args):
        """Returns passed arguments rot13'ed"""
        return post_msg_hook(args.encode('rot13'))

    @botcmd
    def whoami(self, mess, args):
        """Tells you your username"""
        return mess.getFrom().__str__()

    @botcmd
    def cpuinfo( self, mess, args):
        """Displays information about the cpu"""
        cpuinfo = open('/proc/cpuinfo').read().strip()
        return post_msg_hook('%s' % ( cpuinfo, ))
    
    @botcmd
    def uptime( self, mess, args):
        """Displays server uptime"""
        uptime = open('/proc/uptime', 'r')
        uptime_seconds = float(uptime.readline().split()[0])
        uptime_string = str(timedelta(seconds = uptime_seconds))
        return post_msg_hook('%s' % ( uptime_string, ))

    @botcmd
    def blague(self, mess, args):
        """Tells a joke in french"""
        result = subprocess.check_output("$HOME/bin/blague.sh",shell=True)
        return post_msg_hook('%s' % (result, ))

    @botcmd
    def tr(self, mess, args):
        """Random Bitoduc translation (with an arg, translate the IT word into french)"""
        result = subprocess.check_output("$HOME/bin/bitoduc.sh %s" % ( args ),shell=True)
        return post_msg_hook('%s' % (result, ))

    @botcmd
    def spoon(self, mess, args):
        """spoonerism in french (contrepeterie)"""
        result = subprocess.check_output("$HOME/bin/contrepeterie.sh",shell=True)
        return post_msg_hook('%s' % (result, ))

def post_msg_hook(message_string):
    # capslock day
    if datetime.now().strftime('%d-%m') == "22-10":
        return message_string.upper()
    return message_string

def convert_handler(err):
    end = err.end
    return (u' ', end)

def get_out():
    print "bye bye"
    quit()

if __name__ == '__main__':

    if len(sys.argv) != 2:
        print "Usage: %s configuration_file" % sys.argv[0]
        quit()

    config = ConfigParser.RawConfigParser()

    config.read(sys.argv[1])
    username = config.get('credentials', 'username')
    password = config.get('credentials', 'password')
    nickname = config.get('credentials', 'nickname')
    hostname = config.get('server', 'hostname')
    port = config.getint('server', 'port')
    chatroom = config.get('server', 'chatroom')

    codecs.register_error('strict', convert_handler)

    mucbot = Example(username, password, None, False, False, False, None, '',
                     hostname, port, only_direct=False, nickname=nickname, chatroom=chatroom, rnd_max=25)
    mucbot.muc_join_room(chatroom, nickname, "" )
    mucbot.serve_forever(disconnect_callback=get_out)
