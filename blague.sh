#!/bin/bash

blague="$(wget -t 1 -T 5 "https://www.gohumour.com/page/$(( RANDOM % 355 + 1))" -O- 2>/dev/null | \
	html2text -utf8 -width 80000 | sed '0,/    \* Proverbe_dr/d' | sed -n '/ Navigation des articles /q;p')"

nb="$(printf "%s\\n" "$blague" | grep '^\*\*\*\*\* ' | wc -l)"

if [ -z "$nb" ]; then
	echo "erreur"
	exit
fi
if [ $nb -lt 1 ]; then
	echo "erreur"
	exit
fi

rand=$(( RANDOM % nb ))

for i in $(seq 1 $rand); do
	blague="$(printf "%s\n" "$blague" | sed '0,/votes)/d')"
done

printf "%s\n" "$blague" | sed -n '/ votes)/q;p' | head -n -2
