#!/bin/sh

page="$(wget -t 1 -T 5 "https://bitoduc.fr/" -O- 2>/dev/null | grep 'class="mot-')"

if [ $# -eq 0 ]; then
	mot_anglais="$(printf "%s\n" "$page" | grep 'class="mot-anglais"' | sort -R | head -n 1 | sed 's@.*mot-anglais">\(.*\)</.*@\1@')"
	if [ -z "$mot_anglais" ]; then
		printf "aucun mot trouvé\n"
		exit
	else
		printf "%s : " "$mot_anglais"
	fi
else
	mot_anglais="$1"
fi
tr="$(printf "%s\n" "$page" | grep -i -A1 -F "class=\"mot-anglais\">$mot_anglais" | head -n 2 | tail -n 1 | sed 's@.*mot-français">\(.*\)</.*@\1@')"
if [ -z "$tr" ]; then
	printf "aucun mot trouvé\n"
	exit
else
	printf "%s\n" "$tr"
fi
