
CC=gcc
CFLAGS=-Wall -Wextra -std=gnu18 -O2 
LDFLAGS:=$(shell pkg-config --cflags --libs libstrophe)

PROG=vince_bot

all: $(PROG)

$(PROG): $(PROG).c
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

clean:
	rm $(PROG)
