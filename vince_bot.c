/*
 * vince_bot.c
 * Based on:
 *
 * bot
 ** libstrophe XMPP client library -- basic usage example
 **
 ** Copyright (C) 2005-2009 Collecta, Inc.
 **
 **  This software is provided AS-IS with no warranty, either express
 **  or implied.
 **
 ** This program is dual licensed under the MIT and GPLv3 licenses.
 */

/* simple bot example
 **
 ** This example was provided by Matthew Wild <mwild1@gmail.com>.
 **
 ** This bot responds to basic messages and iq version requests.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/syslog.h>
#include <strophe.h>
#include <regex.h>
#include <stdbool.h>
#include <locale.h>
#include <ctype.h>
#include <wchar.h>
#include <wctype.h>

const int global_prio = LOG_DEBUG;

#define UNUSED(x) x __attribute__ ((unused))
#define __maybe_unused __attribute__((unused))

#define RANDOM_MAX  ((2LL << 31) - 1)

#define ARRAY_SZ(x) (sizeof(x) / sizeof((x)[0]))
#define ARRAY_SIZE(x) ARRAY_SZ(x)

#define LOG(p, string, ...)							\
	do {									\
		if (p <= global_prio) {						\
			fprintf(stderr, "Prio=%d: %s(%d) %s " string "\n",	\
				LOG_PRI(p), __FILE__, __LINE__,			\
				 __func__, ## __VA_ARGS__);			\
		}								\
	} while(0)

#define LOG_ERRNO(p, string, ...) \
	LOG(p, string ": %s (%d)", ## __VA_ARGS__, strerror(errno), errno)

static const char adem_pattern[] =
"(^(a|à) ?dem.*)|(^tcho$)|(^bon ?w.*)|(^bonne soir(é|e)e)|(\\bcassos\\b)";

static const char procedures_pattern[] =
"(^[0-9]+ ?s$)|(?:^[0-9]+ ?min.*)|(^h[0-9]+)";

static const char *procedures_str_resp[] = {
	"putain, mais on chie sur les procedures là !!!",
	"pffffff !!!",
	"franchement, c'est abusé !",
	"ok, mais on timeout à 30 alors !!!",
	"Nan, c'est maintenant ! benoît: go",
	NULL
};

static const char cedric_pattern[] = {
	"^cedric: [!@#%]"
};

static const char lolo_pattern[] = {
	"^lolo: [!@#%]"
};

static const char *lolo_str_resp[] = {
	"lolo: !!!!!!!",
	"lolo: !$%#+@",
	"pffffffff ! lolo !!!",
	"et qui c'est qu'on attend ?! lolo !!!",
	"toujours la même qu'on attend !",
	"putain ! lolo !!! ",
	"lolo: c'est pas le moment de shaper !!!! ",
	"lolo: bordel !",
	NULL
};

static const char *cedric_str_resp[] = {
	"cedric: !!!!!!!",
	"cedric: !$%#+@",
	"pffffffff ! cedric !!!",
	"et qui c'est qu'on attend ?! cedric !!!",
	"toujours le même qu'on attend !",
	"putain ! cedric!!! ",
	"cedric: c'est pas le moment de shaper !!!! ",
	"cedric: bordel !",
	NULL
};

static const char *yo_str[] = {
	"yo",
	"salut",
	"slt",
	"plop",
	"salut salut",
	"bonjour",
	"Olà",
	"'lut",
	"hello",
	"chivers",
	"bonjour bonjour",
	NULL
};

static const char direct_str_pattern[] = "^bob[^a-zA-Z]";

static const char blague_pattern[] = "blague";

static const char spoon_pattern[] = "spoon";

static const char fillon_pattern[] = "fillon";

static const char reunion_pattern[] = "r(é|e)union";

static const char dtc_pattern[] = " o(ù|u) \\?$";

static const char *fillon_resp[] = {
	"ET ALORS ?!",
	"REND L'ARGENT !",
	NULL
};

static const char coin_pattern[] = "^(c|p)(oin|an) ?!";

/* yes, bob is not very good at this game */
static const char *coin_resp[] = {
	"coin !",
	"poin !",
	"can !",
	"pan !",
	NULL
};

static const char *go_str[] = {
	"go",
	NULL
};

static const char *bitoduc_str[] = {
	"tr",
	NULL
};

static const char *rot13_str[] = {
	"rot13",
	NULL
};

static const char *help_str[] = {
	"help",
	NULL
};

static const char *help_resp[] = {
	"démerdes-toi tout seul !",
	"chuis pas ta mère !",
	"le noob !",
	"pfffff ! j'ai pas que ça à faire.",
	"RTFM ! Y a plus qu'à l'écrire !",
	"... I need somebody HELP !",
	"je ne serais pas toujours là pour t'aider tu sais...",
	"Demerdare vobis",
	NULL
};

static const char *direct_str_resp[] = {
	"ben chais pas",
	"parles-moi pas toi !",
	"kestuveux ?!",
	"vas-y lâche-moi !",
	"putain, j'dormais...",
	"pffff ! mais qu'est-ce j'en sais moi ?!",
	"honnêtement, je m'en bats les couilles.",
	"qu'est-ce que tu veux que ça me fasse ?",
	"et qu'est-ce tu veux que j'te dise ?",
	"t'sais quoi ? ça m'en touche une sans faire bouger l'autre !",
	"Parle à ma main !",
	"qui me parle ?",
	"toi-même !",
	"cébon! ça va, j'ai compris.",
	"roger.",
	"lapin cômpri",
	"cestcuiquiditquiyest !",
	"miroir incassable !",
	"Wouhaa ! mais c'est génial !",
	"Putain, c'est trop d'la balle",
	"c'est quoi la question ?",
	"je bois les mots à tes lèvres tel cendrillon la sève au gland du prince charmant",
	"<Abraham Simpson>Faut que j'aille prendre mes médicaments</Abraham Simpson>",
	"j'avoue",
	"Je ne pine rien à ce que tu me baves",
	"J'peux pas t'laisser dire ça.",
	"... COMME PAR HASARD !",
	"C'est ce qu'on veut nous faire croire...",
	"Coïncidence ? Je ne crois pas.",
	"Allez c'est ça, retourne regarder BFM",
	"Reste dans ton monde de bisounours.",
	"Les medias c'est tous les mêmes...",
	"On ne nous dit pas tout...",
	"Toutes les infos sont sur YouTube !",
	"La vérité dérange.",
	"mais renseignes-toi vraiment.",
	"Fais tes recherches.",
	NULL
};

static const char *bye_str[] = {
	"ciao", "tcho", "a+", "à plus dans l'bus", "bye",
	"salut", "à c'tantot dans l'métro",
	NULL
};

static const char *adem_str[] = {
	"adem", "à demain dans l'train", "à demain", "bonne soirée",
	NULL
};

static const char *wuik_str[] = {
	"Bon week-end", "bon wuik", "à lundi",
	NULL
};

static const char *ok_str[] = {
	"c'est pas faux",
	"ok",
	"c'est pas con",
	"j'suis pour",
	"moi aussi !",
	"kler.",
	"grav'",
	"sérieux ?",
	"arf.",
	"et c'est bien légitime.",
	"putain, c'est pas passé loin.",
	"+1",
	NULL
};

static const char *nok_str[] = {
	"j'peux pas te laisser dire ça",
	"FOUTAISES !",
	"mais c'est n'importe quoi !!!",
	"tu dis que d'la merde toi !",
	"sans moi",
	"weekly...",
	"la loose...",
	"tu t'es relu quand t'as bu ?",
	"et ben on est pas rendu avec ça !",
	"peux pas, j'ai poney",
	NULL
};

static const char *elec_str[] = {
	"et t'as essayé avec une capa de charge ?",
	"tu le montes en darlington et ça passe !",
	"mais n'imp' ! Fais un push-pull plutôt !",
	"t'as qu'à winker !",
	"n'oublie pas la diode d'inversion de polarité !",
	"Il manque une diode de roue-libre !",
	NULL
};

static const char *insult_str[] = {
	"oh putain mais ta gueule !",
	"salope, salope, salope !",
	"ducon !",
	"et dans l'cul la balayette !",
	"t'es vraiment trop con toi !",
	"RTFM !",
	NULL
};

static const char *conspiracy_str[] = {
	"... COMME PAR HASARD !",
	"C'est ce qu'on veut nous faire croire...",
	"Coïncidence ? Je ne crois pas.",
	"Allez c'est ça, retourne regarder BFM",
	"Reste dans ton monde de bisounours.",
	"Les medias c'est tous les mêmes...",
	"On ne nous dit pas tout...",
	"Toutes les infos sont sur YouTube !",
	"La vérité dérange.",
	"mais renseignes-toi vraiment.",
	"Fais tes recherches.",
	NULL
};

static const char *reunion_str_resp[] = {
	"Piège à cons !",
	"Putain, mais tu bosses quand ?!",
	"Prends ton pipeau doré !",
	NULL
};

static const char *dtc_str_resp[] = {
	"dtc",
	"... dans ton cul !",
	"avec toi, c'est où tu veux quand tu veux ;)",
	NULL
};

struct config {
	char *jid;
	char *pass;
	char *hostname;
	char *chatroom;
	char *chatroom_nick;
	unsigned short port;
};

struct private_data {
	xmpp_ctx_t *ctx;
	struct config conf;
};

enum match_type {
	PATTERN_FAILED,
	EXACT_MATCH,
	PREFIX_MATCH,
	PATTERN
};

enum modes {
	NONE,
	GO_MODE,
	YO_MODE
};

struct regex_match {
	const char *pattern;
	regex_t regex;
};

struct full_match {
	const char **to_match;
};

enum path_type {
	FULL_PATH,
	HOME_DIR,
};

struct regex_entry {
	char *(*callback)(xmpp_stanza_t *reply,
			  xmpp_conn_t *const conn,
			  const char *from, char *body,
			  struct regex_entry *entry);
	enum modes mode;
	enum match_type type;
	union {
		struct regex_match re;
		struct full_match fm;
	} u;
	const char **responses;
	const char *extern_app;
	enum path_type extern_path_type;
};

static char *rot13_callback(xmpp_stanza_t *reply, xmpp_conn_t *const conn,
			    const char *from, char *body, struct regex_entry *entry);
static char *bitoduc_callback(xmpp_stanza_t *reply, xmpp_conn_t *const conn,
			      const char *from, char *body, struct regex_entry *entry);
static char *adem_callback(xmpp_stanza_t *reply, xmpp_conn_t *const conn,
			   const char *from, char *body, struct regex_entry *entry);
static char *std_rnd_callback(xmpp_stanza_t *reply, xmpp_conn_t *const conn,
			      const char *from, char *body, struct regex_entry *entry);

static struct regex_entry regex_tab[] = {
	{
		.callback = &adem_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = adem_pattern,
			},
		},
	},
	{
		.callback = &std_rnd_callback,
		.mode = YO_MODE,
		.type = PREFIX_MATCH,
		.u = {
			.fm = {
				.to_match = yo_str,
			},
		},
		.responses = yo_str,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = direct_str_pattern,
			},
		},
		.responses = direct_str_resp,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = blague_pattern,
			},
		},
		.extern_app = "bin/blague.sh",
		.extern_path_type = HOME_DIR,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = spoon_pattern,
			},
		},
		.extern_app = "bin/contrepeterie.sh",
		.extern_path_type = HOME_DIR,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = EXACT_MATCH,
		.u = {
			.fm = {
				.to_match = help_str,
			},
		},
		.responses = help_resp,
	},
	{
		.callback = &std_rnd_callback,
		.mode = GO_MODE,
		.type = EXACT_MATCH,
		.u = {
			.fm = {
				.to_match = go_str,
			},
		},
		.responses = go_str,
	},
	{
		.callback = &bitoduc_callback,
		.mode = NONE,
		.type = PREFIX_MATCH,
		.u = {
			.fm = {
				.to_match = bitoduc_str,
			},
		},
		.extern_app = "bin/bitoduc.sh",
		.extern_path_type = HOME_DIR,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = fillon_pattern,
			},
		},
		.responses = fillon_resp,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = coin_pattern,
			},
		},
		.responses = coin_resp,
	},
	{
		.callback = &rot13_callback,
		.mode = NONE,
		.type = PREFIX_MATCH,
		.u = {
			.fm = {
				.to_match = rot13_str,
			},
		},
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = procedures_pattern,
			},
		},
		.responses = procedures_str_resp,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = cedric_pattern,
			},
		},
		.responses = cedric_str_resp,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = lolo_pattern,
			},
		},
		.responses = lolo_str_resp,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = reunion_pattern,
			},
		},
		.responses = reunion_str_resp,
	},
	{
		.callback = &std_rnd_callback,
		.mode = NONE,
		.type = PATTERN,
		.u = {
			.re = {
				.pattern = dtc_pattern,
			},
		},
		.responses = dtc_str_resp,
	},
};

static bool yo_mode;
static bool go_mode;

static char *new_str_format(const char *fmt, ...)
{
	int retval;
	char *dest = NULL;
	va_list args;

	va_start(args, fmt);
	retval = vasprintf(&dest, fmt, args);
	va_end(args);
	if (retval < 0) {
		LOG(LOG_ERR, "asprintf failed on format %s.", fmt);
		free(dest);
		return NULL;
	}

	return dest;
}

static char *str_towupper(const char *src)
{
	char *dest = NULL;
	wchar_t *wcs = NULL;

	size_t len;

	if (setlocale(LC_ALL, "fr_FR.UTF-8") == NULL) {
		goto out;
	}

	len = mbstowcs(NULL, src, 0);
	if (len == (size_t) -1) {
		goto out;
	}

	wcs = calloc(len + 1, sizeof(*wcs));
	if (wcs == NULL) {
		goto out;
	}

	if (mbstowcs(wcs, src, len + 1) == (size_t) -1) {
		goto out;
	}
	for (size_t i = 0; i < len; i++) {
		wcs[i] = towupper(wcs[i]);
	}

	len = wcstombs(NULL, wcs, 0);
	if (len == (size_t) -1) {
		goto out;
	}

	dest = calloc(len + 1, sizeof(*dest));
	if (dest == NULL) {
		goto out;
	}

	if (wcstombs(dest, wcs, len) == (size_t) -1) {
		goto out;
	}
out:
	if (dest == NULL)
		dest = strdup("");
	free(wcs);
	return dest;
}

static int version_handler(xmpp_conn_t *const conn,
			   xmpp_stanza_t *const stanza,
			   void *const userdata)
{
	xmpp_stanza_t *reply, *query, *name, *version, *text;
	const char *ns;
	struct private_data *pdata = userdata;
	xmpp_ctx_t *ctx = pdata->ctx;

	printf("Received version request from %s\n", xmpp_stanza_get_from(stanza));

	reply = xmpp_stanza_reply(stanza);
	xmpp_stanza_set_type(reply, "result");

	query = xmpp_stanza_new(ctx);
	xmpp_stanza_set_name(query, "query");
	ns = xmpp_stanza_get_ns(xmpp_stanza_get_children(stanza));
	if (ns) {
		xmpp_stanza_set_ns(query, ns);
	}

	name = xmpp_stanza_new(ctx);
	xmpp_stanza_set_name(name, "name");
	xmpp_stanza_add_child(query, name);
	xmpp_stanza_release(name);

	text = xmpp_stanza_new(ctx);
	xmpp_stanza_set_text(text, "libstrophe example bot");
	xmpp_stanza_add_child(name, text);
	xmpp_stanza_release(text);

	version = xmpp_stanza_new(ctx);
	xmpp_stanza_set_name(version, "version");
	xmpp_stanza_add_child(query, version);
	xmpp_stanza_release(version);

	text = xmpp_stanza_new(ctx);
	xmpp_stanza_set_text(text, "1.0");
	xmpp_stanza_add_child(version, text);
	xmpp_stanza_release(text);

	xmpp_stanza_add_child(reply, query);
	xmpp_stanza_release(query);

	xmpp_send(conn, reply);
	xmpp_stanza_release(reply);
	return 1;
}


static const char *get_nick_from_jid(const char *jid)
{
	char *nick = strchr(jid, '/');
	return nick ? (nick + 1) : jid;
}

static long get_random_between(long min_incl, long max_incl)
{
	int limit = max_incl - min_incl;
	int divisor = RAND_MAX / ( limit + 1 );
	int result;

	do {
		result = rand() / divisor;
	} while (result > limit);

	return result + min_incl;
}

static unsigned get_array_sz(const char **null_terminated_array)
{
	unsigned array_sz = 0;

	for (const char **ptr = null_terminated_array; *ptr != NULL; ptr++) {
		array_sz++;
	}

	return array_sz;
}

static char *bitoduc_callback(UNUSED(xmpp_stanza_t *reply), UNUSED(xmpp_conn_t *const conn),
			      UNUSED(const char *from), char *body, UNUSED(struct regex_entry *entry))
{
	char *arg = NULL;
	char *cmd = NULL;
	FILE *f = NULL;
#define BITODUC_BUF_SZ 255
	char *buf = NULL;
	bool success = false;
	int nb;

	if (entry->extern_app == NULL) {
		return NULL;
	}

	arg = index(body, ' ');
	if (arg == NULL) {
		arg = "";
	} else {
		arg++;
		/*
		 * This could be a huge security breach.
		 * So we are limiting the input to a single
		 * alpha argument.
		 */
		for (nb = 0; (nb < 64) && (arg[nb] != '\0'); nb++) {
			if (!isalpha(arg[nb])) {
				break;
			}
		}
		arg[nb] = '\0';
	}

	if (entry->extern_path_type == HOME_DIR) {
		char *home = getenv("HOME");
		if (home != NULL) {
			cmd = new_str_format("%s/%s %s",
					     home, entry->extern_app, arg);
		} else {
			LOG(LOG_ERR, "No environment variable HOME");
			goto out;
		}
	} else {
		cmd = new_str_format("%s %s", entry->extern_app, arg);
	}

	f = popen(cmd, "r");
	if (!f) {
		LOG_ERRNO(LOG_ERR, "error opening pipe on command %s", cmd);
		goto out;
	}

	buf = malloc(BITODUC_BUF_SZ + 1);
	nb = fread(buf, 1, BITODUC_BUF_SZ, f);
	if (nb > 0) {
		buf[nb] = '\0';
		success = true;
	}
out:
	free(cmd);
	if (f)
		pclose(f);

	if (!success) {
		free(buf);
		buf = NULL;
	}

	return buf;
}


static char *adem_callback(UNUSED(xmpp_stanza_t *reply), UNUSED(xmpp_conn_t *const conn),
			   const char *from, UNUSED(char *body), UNUSED(struct regex_entry *entry))
{
	char *resp;
	struct tm *tm;
	unsigned bye_sz;
	unsigned adem_sz;
	unsigned wuik_sz;
	unsigned idx;
	bool cab_mode = false;
	time_t now;

	now = time(NULL);
	tm = localtime(&now);

	if (tm->tm_hour < 15) {
		/* too soon in the day */
		return NULL;
	}

	bye_sz = get_array_sz(bye_str);
	adem_sz = get_array_sz(adem_str);
	wuik_sz = get_array_sz(wuik_str);

	if (strcmp(from, "caboulot") == 0) {
		cab_mode = true;
	}

	/* sunday->thursday */
	if (((tm->tm_wday < 4) && !cab_mode) ||
	    ((tm->tm_wday < 3) && cab_mode)) {
		idx = get_random_between(0, bye_sz + adem_sz - 1);
		if (idx < bye_sz) {
			return new_str_format("%s %s", bye_str[idx], from);
		} else {
			idx -= bye_sz;
			return new_str_format("%s %s", adem_str[idx], from);
		}
	}

	/* friday */
	if (((tm->tm_wday == 5) && !cab_mode) ||
	    ((tm->tm_wday == 4) && cab_mode)) {
		idx = get_random_between(0, bye_sz + wuik_sz - 1);
		if (idx < bye_sz) {
			return new_str_format("%s %s", bye_str[idx], from);
		} else {
			idx -= bye_sz;
			resp = new_str_format("%s %s", wuik_str[idx], from);
			if (resp && cab_mode) {
				char *ptr;
				ptr = strstr(resp, "lundi");
				if (ptr) {
					*ptr++ = 'm';
					*ptr++ = 'a';
					*ptr++ = 'r';
				}
			}
			return resp;
		}
	}

	return strdup("bonjoir");
}

static char *exec_extern_app(struct regex_entry *entry)
{
	char *cmd = NULL;
	FILE *f = NULL;
#define EXECAPP_BUF_SZ 32767
	char *buf = NULL;
	bool success = false;
	int nb;


	if (entry->extern_path_type == HOME_DIR) {
		char *home = getenv("HOME");
		if (home != NULL) {
			cmd = new_str_format("%s/%s", home, entry->extern_app);
		} else {
			LOG(LOG_ERR, "No environment variable HOME");
			goto out;
		}
	} else {
		cmd = strdup(entry->extern_app);
	}

	f = popen(cmd, "r");
	if (!f) {
		LOG_ERRNO(LOG_ERR, "error opening pipe on command %s", cmd);
		goto out;
	}

	buf = malloc(EXECAPP_BUF_SZ + 1);
	nb = fread(buf, 1, EXECAPP_BUF_SZ, f);
	if (nb > 0) {
		buf[nb] = '\0';
		success = true;
	}

out:
	if (f)
		pclose(f);
	free(cmd);

	if (!success) {
		free(buf);
		buf = NULL;
	}

	return buf;
}

static char *std_rnd_callback(UNUSED(xmpp_stanza_t *reply), UNUSED(xmpp_conn_t *const conn),
			      UNUSED(const char *from), UNUSED(char *body), struct regex_entry *entry)
{
	unsigned array_sz;

	if (entry->mode == YO_MODE) {
		if (yo_mode)
			return NULL;
	}

	if (entry->mode == GO_MODE) {
		if (go_mode)
			return NULL;
	}

	if (entry->responses != NULL) {

		array_sz = get_array_sz(entry->responses);

		return strdup(entry->responses[get_random_between(0, array_sz - 1)]);
	}

	if (entry->extern_app != NULL) {
		return exec_extern_app(entry);
	}

	return NULL;
}

static char *random_response(UNUSED(xmpp_stanza_t *reply), UNUSED(xmpp_conn_t *const conn),
			     UNUSED(const char *from), UNUSED(char *body))
{
	unsigned nb_sentences = 0;
	unsigned rnd;
	const char ***ppp;
	const char **pp;
	const char **sentences_array[] = {
		ok_str,
		nok_str,
		insult_str,
		elec_str,
		conspiracy_str,
		NULL
	};

	for (ppp = sentences_array; *ppp != NULL; ppp++) {
		for (pp = *ppp; *pp != NULL; pp++) {
			nb_sentences++;
		}
	}
	rnd = get_random_between(0, nb_sentences - 1);

	for (ppp = sentences_array; *ppp != NULL; ppp++) {
		for (pp = *ppp; *pp != NULL; pp++) {
			if (rnd == 0) {
				return strdup(*pp);
			}
			rnd--;
		}
	}
	return NULL;
}

static char *rot13(const char *src)
{
	char *dest = strdup(src);
	char *dptr = dest;
	const char *sptr;

	for (sptr = src; *sptr != '\0'; sptr++) {
		char c = *sptr;
		if (c >= 'a' && c < 'n') c += 13;
		else if (c >= 'n' && c <= 'z') c -= 13;
		else if (c >= 'A' && c < 'N') c += 13;
		else if (c >= 'N' && c <= 'Z') c -= 13;
		*dptr++ = c;
	}
	return dest;
}

static char *rot13_callback(UNUSED(xmpp_stanza_t *reply), UNUSED(xmpp_conn_t *const conn),
			    UNUSED(const char *from), char *body, UNUSED(struct regex_entry *entry))
{
	char *arg = NULL;
	char *buf = NULL;
	bool success = false;

	arg = index(body, ' ');
	if (arg == NULL) {
		goto out;
	} else {
		arg++;
	}

	buf = rot13(arg);

	success = true;
out:
	if (!success) {
		free(buf);
		buf = NULL;
	}

	return buf;
}

// conn DEBUG SENT: <message id="purple5eedbe76" to="mouches@conference.rgenoud.crabdance.com/rico" type="groupchat"><body>touducul to you too!</body></message>

int message_handler(xmpp_conn_t *const conn,
		    xmpp_stanza_t *const stanza,
		    void *const userdata)
{
	static unsigned random_resp;
	struct private_data *pdata = userdata;
	xmpp_ctx_t *ctx = pdata->ctx;
	struct config *conf = &(pdata->conf);
	xmpp_stanza_t *body, *reply;
	const char *type;
	char *intext, *replytext = NULL;
	const char *to, *from;
	int quit = 0;
	unsigned i = (unsigned)-1;

	/* This is how often the bot intervenes in the conversation */
	if (random_resp == 0) {
		random_resp = (unsigned) get_random_between(1, 25);
	}

	/* we are only interested in messages with text in them */
	body = xmpp_stanza_get_child_by_name(stanza, "body");
	if (body == NULL)
		return 1;

	type = xmpp_stanza_get_type(stanza);
	if (type != NULL && strcmp(type, "error") == 0)
		return 1;

	intext = xmpp_stanza_get_text(body);

	from = get_nick_from_jid(xmpp_stanza_get_from(stanza));
	printf("Incoming message from %s (%s) : %s\n", xmpp_stanza_get_from(stanza),
	       from, intext);

	if ((from != NULL) && (strcmp(from, "bob") == 0)) {
		/* don't reply to ourselves ! */
		return 1;
	}

	reply = xmpp_stanza_reply(stanza);
	if (xmpp_stanza_get_type(reply) == NULL)
		xmpp_stanza_set_type(reply, "chat");

	to = xmpp_stanza_get_to(reply);

	/* We have to reply to the chatroom */
	if ((to != NULL) && (conf->chatroom != NULL) &&
	    (strncmp(to, conf->chatroom, strlen(conf->chatroom)) == 0)) {
		xmpp_stanza_set_to(reply, conf->chatroom);
	}

	if (strcmp(intext, "quit1234567890") == 0) {
		replytext = strdup("bye!");
		quit = 1;
	} else {
		for (i = 0; i < ARRAY_SZ(regex_tab); i++) {
			if (regex_tab[i].type == PATTERN) {
				struct regex_match *re = &(regex_tab[i].u.re);

				if (regexec(&(re->regex), intext, 0, NULL, 0)) {
					LOG(LOG_DEBUG, "regexec failed on %s / %s",
					    re->pattern, intext);
					continue;
				}
				/* regex succeeds */
				if (regex_tab[i].callback) {
					replytext = regex_tab[i].callback(reply,
									  conn,
									  from,
									  intext,
									  regex_tab + i);
					goto end_loop;
				}
			}
			if (regex_tab[i].type == EXACT_MATCH) {
				const char **pptr = regex_tab[i].u.fm.to_match;
				for (; *pptr != NULL; pptr++) {
					if (strcasecmp(*pptr, intext) == 0) {
						replytext = regex_tab[i].callback(reply,
										  conn,
										  from,
										  intext,
										  regex_tab + i);
						goto end_loop;
					}
				}
			}
			if (regex_tab[i].type == PREFIX_MATCH) {
				const char **pptr = regex_tab[i].u.fm.to_match;
				for (; *pptr != NULL; pptr++) {
					LOG(LOG_DEBUG, "check %s / %s",
					    *pptr, intext);
					if (strncasecmp(*pptr, intext, strlen(*pptr)) == 0) {
						LOG(LOG_DEBUG, "match !!");
						replytext = regex_tab[i].callback(reply,
										  conn,
										  from,
										  intext,
										  regex_tab + i);
						goto end_loop;
					}
				}
			}
		}
	}
end_loop:
	if (i < ARRAY_SZ(regex_tab)) { /* something has matched */
		yo_mode = (regex_tab[i].mode == YO_MODE);
		go_mode = (regex_tab[i].mode == GO_MODE);
		goto send_message;
	}

	/* else, nothing matched */
	yo_mode = false;
	go_mode = false;
	random_resp--;

	if (random_resp == 0) {
		replytext = random_response(reply, conn, from, intext);
	}

send_message:
	xmpp_free(ctx, intext);
	if (replytext != NULL) {
		struct tm *tm;
		time_t now = time(NULL);
		tm = localtime(&now);
		if ((tm->tm_mday == 22) && (tm->tm_mon == 9)) { // 22 october : CAPS LOCK DAY
			char *REPLYTEXT = str_towupper(replytext);
			free(replytext);
			replytext = REPLYTEXT;
		}
		xmpp_message_set_body(reply, replytext);

		usleep(get_random_between(0, 3000000));

		xmpp_send(conn, reply);
	}
	xmpp_stanza_release(reply);
	free(replytext);

	if (quit)
		xmpp_disconnect(conn);

	return 1;
}

int send_muc_presence(xmpp_conn_t *const conn, const char *from, void *const userdata)
{
	struct private_data *pdata = userdata;
	xmpp_ctx_t *ctx = pdata->ctx;
	struct config *conf = &(pdata->conf);
	xmpp_stanza_t *presence, *x;
	const char *ns = "http://jabber.org/protocol/muc";
	char *to = NULL;

	if (conf->chatroom == NULL) {
		return 1;
	}

	asprintf(&to, "%s/%s", conf->chatroom, "bob");

	presence = xmpp_stanza_new(ctx);
	xmpp_stanza_set_name(presence, "presence");
	xmpp_stanza_set_to(presence, to);
	xmpp_stanza_set_from(presence, from);

	x = xmpp_stanza_new(ctx);
	xmpp_stanza_set_name(x, "x");
	xmpp_stanza_set_ns(x, ns);
	xmpp_stanza_add_child(presence, x);
	xmpp_stanza_release(x);

	xmpp_send(conn, presence);
	xmpp_stanza_release(presence);
	free(to);
	return 1;
}
/*
 * conn DEBUG SENT: <presence to="mouches@conference.rgenoud.crabdance.com" from="bot@rgenoud.crabdance.com/KPR3Bf_z"><x xmlns="http://jabber.org/protocol/muc"/></presence>
 * xmpp DEBUG RECV: <presence type="error" to="bot@rgenoud.crabdance.com/KPR3Bf_z" from="mouches@conference.rgenoud.crabdance.com"><error type="cancel"><service-unavailable xmlns="urn:ietf:params:xml:ns:xmpp-stanzas"/></error></presence>
 */

int presence_handler(xmpp_conn_t *const conn,
		     xmpp_stanza_t *const stanza,
		     void *const userdata)
{
	struct private_data *pdata = userdata;
	struct config *conf = &(pdata->conf);
	const char *to;
	const char *from;

	to = xmpp_stanza_get_to(stanza);
	if (to != NULL)
		return 1;

	from = xmpp_stanza_get_from(stanza);
	if ((from != NULL) && (strncmp(from, conf->jid, strlen(conf->jid)) == 0)) {
		send_muc_presence(conn, from, userdata);
	}
	return 1;
}

/* define a handler for connection events */
void conn_handler(xmpp_conn_t *const conn,
		  const xmpp_conn_event_t status,
		  const int error,
		  xmpp_stream_error_t *const stream_error,
		  void *const userdata)
{
	struct private_data *pdata = userdata;
	xmpp_ctx_t *ctx = pdata->ctx;

	(void)error;
	(void)stream_error;

	if (status == XMPP_CONN_CONNECT) {
		xmpp_stanza_t *pres;
		fprintf(stderr, "DEBUG: connected\n");
		xmpp_handler_add(conn, version_handler, "jabber:iq:version", "iq", NULL,
				 userdata);
		xmpp_handler_add(conn, message_handler, NULL, "message", NULL, userdata);
		xmpp_handler_add(conn, presence_handler, NULL, "presence", NULL, userdata);

		/* Send initial <presence/> so that we appear online to contacts */
		pres = xmpp_presence_new(ctx);
		xmpp_send(conn, pres);
		xmpp_stanza_release(pres);
//		(void) send_muc_presence(conn, userdata);
	} else {
		fprintf(stderr, "DEBUG: disconnected\n");
		xmpp_stop(ctx);
	}
}

/*
 * TODO: MUC: https://xmpp.org/extensions/xep-0045.html#enter
 * Example 18. User Seeks to Enter a Room (Multi-User Chat)¶
 *
 * <presence
 *    from='hag66@shakespeare.lit/pda'
 *    id='n13mt3l'
 *    to='coven@chat.shakespeare.lit/thirdwitch'>
 *  <x xmlns='http://jabber.org/protocol/muc'/>
 * </presence>
 * In this example, a user with a full JID of "hag66@shakespeare.lit/pda" has requested to enter the room "coven" on the "chat.shakespeare.lit" chat service with a room nickname of "thirdwitch".
 */

int read_config(const char *conffile, struct config *c)
{
	FILE *f = NULL;
	int err = 1;
	char *key = NULL;
	char *value = NULL;
	int nb;

	if (access(conffile, R_OK) != 0) {
		LOG_ERRNO(LOG_ERR, "Unable to access %s", conffile);
		goto out;
	}
	f = fopen(conffile, "r");
	if (f == NULL) {
		LOG_ERRNO(LOG_ERR, "Unable to open %s", conffile);
		goto out;
	}

	while (1) {
		nb = fscanf(f, " %ms = %ms\n", &key, &value);
		if (nb == EOF) {
			break;
		}
		if (nb != 2) {
			continue;
		}

		if (strcmp(key, "jid") == 0) {
			free(c->jid);
			c->jid = value;
			value = NULL;
		} else if (strcmp(key, "password") == 0) {
			free(c->pass);
			c->pass = value;
			value = NULL;
		} else if (strcmp(key, "hostname") == 0) {
			free(c->hostname);
			c->hostname = value;
			value = NULL;
		} else if (strcmp(key, "chatroom") == 0) {
			free(c->chatroom);
			c->chatroom = value;
			value = NULL;
		} else if (strcmp(key, "port") == 0) {
			c->port = atoi(value);
		}
		free(key);
		free(value);
		key = NULL;
		value = NULL;
	}

	if (c->jid && c->pass) {
		err = 0;
	}

out:
	if (f) {
		fclose(f);
	}

	if (err) {
		free(c->jid);
		free(c->pass);
		free(c->hostname);
		free(c->chatroom);
		c->jid = NULL;
		c->pass = NULL;
		c->hostname = NULL;
		c->chatroom = NULL;
		c->port = 0;
	}

	return err;
}

int main(int argc, char **argv)
{
	struct private_data priv = {
		.ctx = NULL,
		.conf = {
			.jid = NULL,
			.pass = NULL,
			.hostname = NULL,
			.chatroom = NULL,
			.port = 0
		},
	};
	xmpp_conn_t *conn;
	xmpp_log_t *log;
	struct config *conf = &(priv.conf);
	int err;

	if (argc < 1) {
		return 1;
	}

	if (argc != 2) {
		fprintf(stderr, "Usage: %s config_file\n\n", argv[0]);
		return 1;
	}

	srandom(time(NULL));

	/* compile regexes */
	for (unsigned i = 0; i < ARRAY_SZ(regex_tab); i++) {
		if (regex_tab[i].type == PATTERN) {
			struct regex_match *re = &(regex_tab[i].u.re);
			if (re->pattern != NULL) {
				if (regcomp(&(re->regex), re->pattern,
					    REG_ICASE | REG_NOSUB | REG_EXTENDED)) {
					LOG(LOG_ERR, "regcomp failed on %s", re->pattern);
					regex_tab[i].type = PATTERN_FAILED;
				}
			}
		}
	}

	/* init library */
	xmpp_initialize();

	err = read_config(argv[1], conf);
	if (err) {
		goto out;
	}

	/* pass NULL instead to silence output */
	log = xmpp_get_default_logger(XMPP_LEVEL_DEBUG);
	/* create a context */
	priv.ctx = xmpp_ctx_new(NULL, log);

	/* create a connection */
	conn = xmpp_conn_new(priv.ctx);

	/*
	 * also you can disable TLS support or force legacy SSL
	 * connection without STARTTLS
	 *
	 * see xmpp_conn_set_flags() or examples/basic.c
	 */

	/* setup authentication information */
	xmpp_conn_set_jid(conn, conf->jid);
	xmpp_conn_set_pass(conn, conf->pass);

	/* initiate connection */
	xmpp_connect_client(conn, conf->hostname, conf->port, conn_handler, &priv);

	/* enter the event loop -
	   our connect handler will trigger an exit */
	xmpp_run(priv.ctx);

	/* release our connection and context */
	xmpp_conn_release(conn);
	xmpp_ctx_free(priv.ctx);

	/* final shutdown of the library */
	xmpp_shutdown();

out:
	free(conf->jid);
	free(conf->pass);
	free(conf->hostname);
	return err;
}
